<?php require_once 'inc/top.php';?>

<?php

if (!isset($_SESSION['kori'])) {
  $_SESSION['kori'] = array();
}


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $kori = $_SESSION['kori'];
  $tuote = filter_input(INPUT_POST,'tuote',FILTER_SANITIZE_STRING);
  array_push($kori,$tuote);
  $_SESSION['kori'] = $kori;
}
?>

<h1>XXL</h1>

<?php
print "<p>Ostoskorissa on " . count($_SESSION['kori']) . " tuotetta</p>";
?>
<a href="ostoskori.php">Ostoskoriin</a>

<form action="<?php print($_SERVER['PHP_SELF']);?>" method="post">
<input name="tuote" type="hidden" value="Halo Down Hoodie, miesten untuvatakki">
<div class="mb-5">
  <img src="img/takki.png">
  <h3>The North Face</h3>
  <p>
  Halo Down Hoodie, miesten untuvatakki
  </p>
  <p>
  70 €
  </p>
  <button class="btn btn-primary">Osta</button>
</div>
</form>

<form action="<?php print($_SERVER['PHP_SELF']);?>" method="post">
<input name="tuote" type="hidden" value="Vision II GTX, miesten retkeilykengät">
<div>
  <img src="img/kengat.png">
  <h3>Haglöfs</h3>
  <p>
  Vision II GTX, miesten retkeilykengät
  </p>
  <p>
  60 €
  </p>
</div>
<button class="btn btn-primary">Osta</button>
</form>
<?php require_once 'inc/bottom.php';?>