<?php require_once 'inc/top.php'; ?>

<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $_SESSION['kori'] = null;
}
?>
<h3>Ostoskori</h3>
<ul>
<?php
$kori = $_SESSION['kori'];
if ($kori) {
  foreach ($kori as $tuote) {
    print "<li>$tuote</li>";
  }
}
?>
</ul>
<form action="<?php $_SERVER['PHP_SELF'];?>" method="post">
<button class="btn btn-primary">Tyhjennä</button>
</form>

<a href="index.php">Takaisin kauppaan</a>
<?php require_once 'inc/bottom.php'; ?>