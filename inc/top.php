<?php
session_start();
session_regenerate_id();
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../font-awesome-4.7.0/css/font-awesome.min">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="jquery.fancybox.min.css">
    

    <title>Kuvagalleria</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-sm bg-secondary">

        <!-- Links -->
          <div class="col-6">
            <a class="nav-link text-white text-left" href="#">Kuvagalleria</a>
          </div>
          <div class="col-6">
            <a class="nav-link text-white text-right" href=add.php>Lisää <i class="fa fa-camera" aria-hidden="true"></i></a>
          </div>
    
      
      </nav>
</body>
<div class="container">
  <div class="row">
    <div class="col">